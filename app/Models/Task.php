<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Task extends Model
{
    public const RULES = [
        'name' => 'required|string',
        'description' => 'required|string',
        /** Можно решить с помощью фасада Rule */
        'status' => 'required|string|in:new,in_progress,completed,canceled',
    ];

    protected $fillable = [
        'name',
        'description',
        'status',
    ];

    protected $casts = [
        'id' => 'int',
        'name' => 'string',
        'description' => 'string',
        'status' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public const FILTERS = [
        'name' => true,
        'status' => true,
        'created_at' => true,
        'updated_at' => true,
    ];
}
