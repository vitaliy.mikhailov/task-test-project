<?php

namespace App\Enums;

/**
 * При изменении значений перечисления, необходимо обновить миграции и валидацию модели
 */
enum TaskStatuses: string
{
    case New = 'new';
    case InProgress = 'in_progress';
    case Completed = 'completed';
    case Canceled = 'canceled';
}
