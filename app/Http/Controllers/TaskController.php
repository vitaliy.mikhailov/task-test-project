<?php

namespace App\Http\Controllers;

use App\Enums\TaskStatuses;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $filters = $request->all();

        $taskBuilder = Task::query();

        foreach ($filters as $key => $value) {
            if (isset(Task::FILTERS[$key]) && in_array($value, ['asc', 'desc'])) {
                $taskBuilder->orderBy($key, $value);
            }
        }

        $tasks = $taskBuilder->paginate(15);

        return response()->json($tasks);
    }

    public function allStatuses(): JsonResponse
    {
        $values = array_column(TaskStatuses::cases(), 'value');
        return response()->json($values);
    }

    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(Task::RULES);
        $task = Task::query()->create($data);
        return response()->json($task, 201);
    }

    public function show(Task $task): JsonResponse
    {
        return response()->json($task);
    }

    public function edit(Task $task): JsonResponse
    {
        return response()->json($task);
    }

    public function update(Request $request, Task $task): JsonResponse
    {
        $data = $request->validate(Task::RULES);
        $task->update($data);
        return response()->json($task);
    }

    public function destroy(Task $task): JsonResponse
    {
        $task->delete();
        return response()->json(null, 204);
    }
}
